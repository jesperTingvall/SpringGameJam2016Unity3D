# Goal
For Spring Game Jam 2016
Lecture: https://www.facebook.com/events/1709320975989447/

## Topics
Unity 3D Editor
Unity Components
Import art assets
Component based script architecture (GetComponent, FindComponent)
Create & delete objects
Prefabs
Colliders
Git & Unity
Animations

By Jesper Tingvall
tingvall.pw