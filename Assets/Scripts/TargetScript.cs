﻿using UnityEngine;
using System.Collections;

public class TargetScript : MonoBehaviour {

	public void Hit() {
		FindObjectOfType<PlayerTank> ().Score ();
		GetComponent<Animator> ().SetTrigger ("death");
	}

	public void Remove() {
		Destroy (gameObject);
	}
}
