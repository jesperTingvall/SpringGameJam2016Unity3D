﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {

	public float speed;

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody> ().velocity = transform.forward * speed;
		Destroy (gameObject, 10);
	
	}

	public void OnCollisionEnter(Collision collision) {

		var target = collision.gameObject.GetComponent<TargetScript> ();

		if (target != null) {
			target.Hit ();
		}

		Destroy (gameObject);
	}

	
	// Update is called once per frame
	void Update () {
	
	}
}
