﻿using UnityEngine;
using System.Collections;

public class PlayerTank : MonoBehaviour {

	public Transform bulletSpawnPos;

	private Animator animator;

	public GameObject bullet;

	private CharacterController character;

	public float speed;
	public float turnSpeed;

	public int score;

	// Use this for initialization
	void Start () {
		character = GetComponent<CharacterController> ();
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		float input = Input.GetAxis ("Vertical");

		animator.SetFloat ("speed", input);

		character.SimpleMove (transform.forward * speed * input);

		transform.Rotate (0, turnSpeed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0);

		if (Input.GetButtonDown ("Fire1")) {
			Instantiate (bullet, bulletSpawnPos.position, bulletSpawnPos.rotation);
		}
	}

	public void Score() {
		score++;
	}
}
